import Axios from 'axios';
import React,{Component} from 'react';
import {Link} from 'react-router-dom';

export default class Navbar extends Component{

    constructor(){
        super();

        this.logout = this.logout.bind(this);

        this.state = {
            loggedIn : false,
            accessToken : null,
            refreshToken : null,
            username : null,
            userId : null
        }
    }

    logout(){
        localStorage.removeItem('accessToken');
        localStorage.removeItem('refreshToken');

        window.location = '/'
    }

    componentDidMount(){
        const accessToken = localStorage.getItem('accessToken');
        const refreshToken = localStorage.getItem('refreshToken');

        // Check authenticate dulu... kalo belum langsung logout
        if(accessToken){
            const authenticateAPI = process.env.REACT_APP_API_BASEURL + process.env.REACT_APP_API_AUTHENTICATE 
            Axios.get(authenticateAPI,{
                headers : {
                    authorization : "Bearer "+accessToken,
                    refreshToken
                }
            })
            .then((result)=>{
                if(result.status === 200){
                    localStorage.setItem('accessToken',result.data.accessToken);
                }
            }).catch((res)=>{
                this.logout()
            })
    
            if(accessToken){
                this.setState({
                    loggedIn : true
                });
            }
        }
    }

    Loginbutton(){
        if(this.state.loggedIn){
            return <button onClick={this.logout} className="btn btn-danger">Logout</button>
        }else{
            return <Link to="/login" className="btn btn-primary">Login</Link>
        }
    }

    MenuList(){
        if(this.state.loggedIn){
            return (
                <>
                    <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Todo's</a>
                        <div className="dropdown-menu">
                            <Link className="dropdown-item" to="/todo">Todo's</Link>
                            <Link className="dropdown-item" to="/todo/item">Todo's Item</Link>
                        </div>
                    </li>
                </>
            )
        }
    }

    render(){
        return (
            <>

            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <a className="navbar-brand" href="/">Todo App</a>
                <button className="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation"></button>
                <div className="collapse navbar-collapse" id="collapsibleNavId">

                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        {this.MenuList()}
                    </ul>
                    
                    {this.Loginbutton()}
                </div>
            </nav>

            </>
        )
    }

}