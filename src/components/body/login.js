import React,{Component} from 'react';
import Axios from 'axios';

export default class Login extends Component{

    constructor(){
        super();

        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeUsername = this.onChangeUsername.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.formValidate = this.formValidate.bind(this);


        this.state = {
            username : '',
            password : '',
            usernameError : '',
            passwordError : '',
            errorLogin : ""
        }
    }


    onChangePassword(e){
        this.setState({
            passwordError : ""
        });
        this.setState({
            password : e.target.value
        });
    }

    onChangeUsername(e){
        this.setState({
            usernameError : ""
        });

        this.formValidate();
        this.setState({
            username : e.target.value
        });
    }

    onSubmit(e){
        e.preventDefault();
        const loginAPI = process.env.REACT_APP_API_BASEURL + '/user/login';
        if(!this.formValidate()){
            return false;
        }

        Axios.post(loginAPI,{
            username : this.state.username,
            password : this.state.password
        }).then((res)=>{
            const loginData = res.data.data; 
            const userData  = loginData.userData;

            localStorage.setItem("accessToken",loginData.accessToken);
            localStorage.setItem("refreshToken",loginData.refreshToken);
            localStorage.setItem("userId",userData.userId)
            localStorage.setItem("username",userData.username);

            window.location = '/';
            
        }).catch((response)=>{
            if(response.response.status === 403){
                this.setState({
                    errorLogin : response.response.data
                });
            }
        });

        
    }

    loginAlert(){
        if(this.state.errorLogin.length > 0){
            return (
                <div className="alert alert-danger">
                    {this.state.errorLogin}          
                </div>
            )
        }
    }

    formValidate(){
        let isValid = true;
        // validate
        if(this.state.username.length <= 0){
            this.setState({
                usernameError : "Username required!"
            })
            isValid = false;
        }

        if(this.state.password.length <= 0){
            this.setState({
                passwordError : "Password required!"
            })
            isValid = false;
        }

        return isValid;
    }

    render(){
        return (
            <div className="row d-flex align-items-center">
                <div className="offset-8 col-4 mt-4">
                    <div className="card">
                        <img className="card-img-top" src="holder.js/100x180/" alt=""/>
                        <div className="card-body">
                            <form className="form-horizontal" onSubmit={this.onSubmit}>

                            {this.loginAlert()}
                            
                            <div className="form-group">
                                <label>Username</label>
                                <input 
                                    type="text" name="username" id="username"
                                    onKeyUp={this.onChangeUsername} className="form-control" 
                                    autoComplete="off"
                                />

                                <small style={{color:"red"}}>
                                    {this.state.usernameError}
                                </small>
                            </div>

                            <div className="form-group">
                                <label>Password</label>
                                <input type="password" name="password" id="password" onKeyUp={this.onChangePassword} className="form-control" placeholder="" aria-describedby="helpId"/>
                                <small style={{color:"red"}}>
                                    {this.state.passwordError}
                                </small>
                            </div>

                            <button className="btn btn-primary" type="submit">Login</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}