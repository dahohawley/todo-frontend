import React from 'react';
import Axios from 'axios';
import Loading from '../../loading';
import $ from 'jquery'


export default class ModalNewItem extends React.Component{
    constructor(props){
        super(props);

        this.submit = this.submit.bind(this)
        this.validate = this.validate.bind(this)
        this.setType = this.setType.bind(this)

        this.state = {
            name : '',
            type : 0,
            recurring : 0,
            errorName : "",
            errorType : "",
            typeDescription : null,
            typeDescriptionClass : "d-none",
            showLoading : false
        }
        
    }

    setType(value){
        this.setState({
            type : value
        });

        switch(value){
            case "0" : 
                this.setState({
                    typeDescriptionClass : "d-none"
                });
            break;
            case "D" :
                this.setState({
                    typeDescription : "Day",
                    typeDescriptionClass : "form-group"
                });
            break;
            case "W" :
                this.setState({
                    typeDescription : "Week",
                    typeDescriptionClass : "form-group"
                });
            break;
            case "M" :
                this.setState({
                    typeDescription : "Month",
                    typeDescriptionClass : "form-group"
                });
            break;
        }

    }

    submit(){
        if(!this.validate()) return;

        const accessToken = localStorage.getItem('accessToken');

        const API_SAVE_ITEM = process.env.REACT_APP_API_BASEURL + process.env.REACT_APP_API_ITEM_CREATE;

        this.setState({
            showLoading : true
        });

        Axios.post(
            API_SAVE_ITEM,
            {
                name : this.state.name,
                type : this.state.type,
                recurring : this.state.recurring
            },
            {
                headers :{
                    authorization : "Bearer "+accessToken
                }
            }
        ).then((res)=>{
            $(".modal").modal('hide');
            this.setState({
                showLoading : false
            })
        })
    }


    validate(){
        let isValid = true;
        if(this.state.name === ''){
            isValid = false;
            this.setState({
                errorName : "Name required!"
            });
        }else{
            this.setState({
                errorName : ""
            });
        }

        if(this.state.type === ''){
            isValid = false;
            this.setState({
                errorType : "Type required!"
            });
        }else{
            this.setState({
                errorType : ""
            });
        }

        return isValid;
    }

    render(){ 
        return(
            <div className="modal fade" id="modal-create-new" tabIndex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                <Loading show={this.state.showLoading} />
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Modal title</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body">
                            <form onSubmit={()=>this.onSubmit} >
                                <input type="hidden" name="_id" id="_id"/>

                                <div className="form-group">
                                    <label htmlFor="">Name</label>
                                    <input 
                                        value={this.state.name}
                                        type="text" 
                                        onChange={(e)=>this.setState({name : e.target.value})} 
                                        className="form-control" 
                                    />
                                    <small className="text-danger">{this.state.errorName}</small>
                                </div>

                                <div className="form-group">
                                    <label htmlFor="">Type</label>
                                    <select 
                                        className="form-control" id="type" 
                                        name="type" 
                                        onChange={(e)=>this.setType(e.target.value)}
                                    >
                                            <option value="0">Choose Type</option>
                                            <option value="D">Daily</option>
                                            <option value="W">Weekly</option>
                                            <option value="M">Monthly</option>
                                    </select>
                                    <small className="text-danger">{this.state.errorType}</small>
                                </div>

                                <div className={this.state.typeDescriptionClass}>
                                    <label htmlFor="">Every</label>
                                    <div className="input-group mb-3">
                                        <input type="number" name="recurring" onChange={(e)=>this.setState({recurring : e.target.value })} className="form-control"/>
                                        <div className="input-group-append">
                                            <span className="input-group-text" id="basic-addon2">{this.state.typeDescription}</span>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" onClick={this.submit} >Save</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}