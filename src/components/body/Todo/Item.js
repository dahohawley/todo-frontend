import Axios from 'axios';
import React from 'react';
import ModalNewItem from './ModalNewItem';
import $ from 'jquery'


export default class Item extends React.Component{

    constructor(){
        super()

        this.deleteItem = this.deleteItem.bind(this);
        this.createNew = this.createNew.bind(this);

        this.state = {
            items : []
        }
    }

    createNew(){
        $("#modal-create-new").modal('show')
    }

    deleteItem(id){
        this.setState({
            items : this.state.items.filter((item)=> item._id !== id)
        })
    }

    componentDidMount(){
        const env = process.env;
        const API_TODO_ITEMS = env.REACT_APP_API_BASEURL + env.REACT_APP_API_ITEM_LIST;
        Axios.get(API_TODO_ITEMS,{
            headers : {
                authorization : "Bearer "+localStorage.getItem('accessToken')
            }
        }).then((res)=>{
            this.setState({
                items : res.data
            })
        });
    }

    render(){
        const Items = this.listItem();

        return (
            <>
                <ModalNewItem/>
                
                <div className="card">
                    <div className="card-header">
                        TodoItem
                    </div>
                    <div className="card-body">
                        <div className="row mb-2">
                            <div className="col-12 d-flex justify-content-end">
                                <button className="btn btn-primary" onClick={()=>this.createNew()} >Create new</button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <ul className="list-group">
                                    {Items}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }

    listItem() {
        return this.state.items.map((item) => {
            let recurringName = '';
            if (item.type === 'D') {
                recurringName = "Daily";
            } else if (item.type === 'W') {
                recurringName = 'Weekly';
            }

            return (
                <li className="list-group-item d-flex justify-content-between" key={item._id}>
                    <div>
                        <p>{item.name} <span className="badge badge-primary">{recurringName}</span> </p>
                    </div>
                    <div className="action">
                        <div className="btn-group">
                            <button className="btn btn-sm btn-info"> Edit</button>
                            <button className="btn btn-sm btn-danger" onClick={() => this.deleteItem(item._id)}>Delete</button>
                        </div>
                    </div>
                </li>
            );
        });
    }
}