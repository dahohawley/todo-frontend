import React,{Component} from 'react';
import { Route } from "react-router-dom";

import Welcome from './body/welcome';
import Login from './body/login'
import Todo from './body/Todo';
import TodoItem from './body/Todo/Item';

export default class Body extends Component{
    render(){
        return (
            <>

            <Route path="/" exact component={Welcome}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/todo" exact component={Todo}></Route>
            <Route path="/todo/item" component={TodoItem}></Route>

            </>
        )
    }
}