import React from 'react';

export default function Loading(props){
    
    const loaderStyle = {
        position: "fixed",
        display: "block",
        width: "100%",
        height: "100%",
        top: "0",
        left: "0",
        right: "0",
        bottom: "0",
        backgroundColor: "rgba(0,0,0,0.3)",
        zIndex: "99999",
        cursor: "pointer",
    }
    const iconStyle = {
        zIndex: "99999",
        color : "white",
        fontSize : "100px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        minHeight: "100vh",

    }

    if(props.show){
        return (
            <>
                <div style={loaderStyle}>
                    <i className="fa fa-spin fa-spinner" style={iconStyle} aria-hidden="true"></i>
                </div>
            </>
        )
    }else{
        return ""
    }
    
}