import Navbar from "./components/navbar";
import { HashRouter as Router } from "react-router-dom";

import Body from './components/body';

function App() {
	return (
		<Router>
			<Navbar />
			<div className="container mt-2">
				<Body/>
			</div>
		</Router>
	)
}

export default App;
